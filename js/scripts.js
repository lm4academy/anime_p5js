var dinos = [];
var counter = 0;
var pos_x = 0;
function setup(){
    createCanvas(800, 400);
    for (let i=1; i<=10; i++){
        dinos.push(loadImage("imgs/Walk ("+i+").png"))
    }
}

function draw(){
    background(220);
    let img = dinos[Math.round(counter/3)%10];
    counter ++;
    image(img, pos_x, 0, img.width/2, img.height/2);
    pos_x += 2;
    if (pos_x >= 800 - img.width /2){
        pos_x = 0;
    }
}